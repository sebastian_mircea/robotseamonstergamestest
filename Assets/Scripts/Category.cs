﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Keeps data related to a category.
/// </summary>
public class Category : MonoBehaviour
{
    /// <summary>
    /// The category name.
    /// </summary>
    [SerializeField]
    private string _name;

    /// <summary>
    /// A list of the options in this category.
    /// </summary>
    [SerializeField]
    private List<CategoryOption> _options;

    /// <summary>
    /// Getter for the category name.
    /// </summary>
    public string Name
    {
        get { return _name; }
    }

    /// <summary>
    /// Returns a dictionary containing the names and sprites coresponding to the options.
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, Sprite> GetOptionsDictionary()
    {
        var optionsDict = new Dictionary<string, Sprite>();
        foreach (var option in _options)
        {
            optionsDict[option.OptionName] = option.Image;
        }
        return optionsDict;
    }
}
