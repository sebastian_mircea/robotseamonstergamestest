﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the menu scene.
/// </summary>
public class SwipeMenuSceneMgr : MonoBehaviour
{
    /// <summary>
    /// The category menu button.
    /// </summary>
    [SerializeField]
    private Button _categoryMenuButton;

    /// <summary>
    /// The category menu scroll view.
    /// </summary>
    [SerializeField]
    private CategoryMenu _categoryMenu;

    /// <summary>
    /// The options menu scroll view.
    /// </summary>
    [SerializeField]
    private OptionsMenu _optionsMenu;

    /// <summary>
    /// The selected options name.
    /// </summary>
    [SerializeField]
    private Text _optionsText;

    [SerializeField]
    private List<Category> _categories;

    /// <summary>
    /// The text of the category menu button - shows the selected category.
    /// </summary>
    private Text _categoryMenuButtonText;

    /// <summary>
    /// Used for initialization.
    /// </summary>
    private void Awake()
    {
        // Add a listener for the category menu button's click event.
        _categoryMenuButton.onClick.AddListener(() => 
        {
            // Show the category menu.
            _categoryMenu.gameObject.SetActive(true/*!_categoryMenu.gameObject.activeInHierarchy*/);
            // Hide the menu button.
            _categoryMenuButton.gameObject.SetActive(false);
        });
        // Get a reference to the button's text component.
        _categoryMenuButtonText = _categoryMenuButton.GetComponentInChildren<Text>(true);
    }

    /// <summary>
    /// Used for initialization.
    /// </summary>
    private void Start()
    {
        // TODO: Remove test code        

        var categorieNamesList = new List<string>();
        foreach (var category in _categories)
        {
            categorieNamesList.Add(category.Name);
        }
        // Listen for category selection changes.
        _categoryMenu.OnItemSelected += CategoryMenu_OnCategorySelected;

        // Add the categories to the menu.
        _categoryMenu.AddCategories(categorieNamesList);

        // Select the first category.
        _categoryMenu.SelectItem(0);

        // Hide the menu and show the menu button.
        HideCategoryMenuAndShowCategoryMenuButton();
    }

    /// <summary>
    /// OnDestroy event handler.
    /// </summary>
    private void OnDestroy()
    {
        // Unregister the event listener.
        _categoryMenu.OnItemSelected -= CategoryMenu_OnCategorySelected;
        _optionsMenu.OnItemSelected -= OptionsMenu_OnOptionSelected;
    }

    /// <summary>
    /// Category Selected event handler.
    /// </summary>
    /// <param name="item"></param>
    private void CategoryMenu_OnCategorySelected(ScrollMenuItem item)
    {
        if (item == null)
        {
            Debug.LogError("Invalid parameter!");
            return;
        }

        // TODO: Remove test code

        // Display the name of the selected category.
        _categoryMenuButtonText.text = item.ItemName;
        // Hide the menu and show the menu button.
        HideCategoryMenuAndShowCategoryMenuButton();

        // Clear any previously added options.
        _optionsMenu.ClearItems();

        Category currentCategory = null;
        foreach (var category in _categories)
        {
            if (category.Name == item.ItemName)
            {
                currentCategory = category;
                break;
            }
        }

        if (currentCategory == null)
        {
            Debug.LogError("Category not found!");
            return;
        }
        
        // Register the item clicked event listener.
        _optionsMenu.OnItemSelected += OptionsMenu_OnOptionSelected;

        // Select the first option.
        _optionsMenu.SelectItem(0);

        //Populate the options menu.
        _optionsMenu.AddOptions(currentCategory.GetOptionsDictionary());
    }

    /// <summary>
    /// Option selected event handler.
    /// </summary>
    /// <param name="item"></param>
    private void OptionsMenu_OnOptionSelected(ScrollMenuItem item)
    {
        if (item == null)
        {
            Debug.LogError("Invalid parameter!");
            return;
        }

        _optionsText.text = item.ItemName;
    }

    /// <summary>
    /// Hides the category menu and shows the category menu button.
    /// </summary>
    private void HideCategoryMenuAndShowCategoryMenuButton()
    {
        // Activate the category menu button.
        _categoryMenuButton.gameObject.SetActive(true);
        // Hide the category menu.
        _categoryMenu.gameObject.SetActive(false);
    }
}
