﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Implementation for category menu items.
/// </summary>
[RequireComponent(typeof(Toggle))]
public class CategoryMenuItem : ScrollMenuItem
{
    /// <summary>
    /// The toggle that represents the category in the menu.
    /// </summary>
    private Toggle _toggle;

    /// <summary>
    /// The button text.
    /// </summary>
    private Text _toggleLabel;

    /// <summary>
    /// Getter for the menu item Toggle component.
    /// </summary>
    public Toggle MenuItemToggle
    {
        get { return _toggle; }
    }

    /// <summary>
    /// Getter and setter for the category name.
    /// </summary>
    public override string ItemName
    {
        get { return _toggleLabel.text; }
        set { _toggleLabel.text = value; }
    }

    /// <summary>
    /// Initialize the menu item.
    /// </summary>
    protected override void Initialize()
    {
        // Get a reference to the button component.
        _toggle = GetComponent<Toggle>();
        // Add a listener for the click events.
        _toggle.onValueChanged.AddListener((value) =>
        {
            if (value)
            {
                CallOnItemClickedEventObservers();
            }
        });
        // Get a reference to the toggle's label.
        _toggleLabel = _toggle.GetComponentInChildren<Text>(true);
    }
}
