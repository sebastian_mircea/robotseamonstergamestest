﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A scrollable menu for the options in a category.
/// </summary>
[RequireComponent(typeof(SnapScrollRect))]
public class OptionsMenu : ScrollMenu
{
    [SerializeField]
    private Button _button;

    /// <summary>
    /// A reference to the snap scroll rect.
    /// </summary>
    private SnapScrollRect _snapScrollRect;

    /// <summary>
    /// Used for initialization.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        _snapScrollRect = GetComponent<SnapScrollRect>();
        _snapScrollRect.OnSwipeUp += SnapScrollRect_OnSwipeUp;

        _button.onClick.AddListener(OnClick);
    }

    /// <summary>
    /// Do what is needed before the object is destroyed.
    /// </summary>
    protected override void OnDestroy()
    {
        base.OnDestroy();
        _snapScrollRect.OnSwipeUp -= SnapScrollRect_OnSwipeUp;
        _button.onClick.RemoveListener(OnClick);
    }

    /// <summary>
    /// Add categories to the menu.
    /// </summary>
    /// <param name="categories">A dictionary of categories to add.</param>
    public void AddOptions(Dictionary<string, Sprite> options)
    {
        foreach (var option in options)
        {
            // Instantiate a menu item prefab for each option in the dictionary.
            var newOption = InstantiateItem() as OptionsMenuItem;
            // Set the name of the option.
            newOption.ItemName = option.Key;
            // Set the image of the option.
            newOption.ItemSprite = option.Value;
        }

        // Initialize the snap scroll rect
        _snapScrollRect.Initialize();
    }

    /// <summary>
    /// SwipeUp event handler.
    /// </summary>
    /// <param name="currentItemIndex">The index of the current item.</param>
    private void SnapScrollRect_OnSwipeUp(int currentItemIndex)
    {
        SelectItem(currentItemIndex);
    }

    /// <summary>
    /// OnClick event handler.
    /// </summary>
    private void OnClick()
    {
        SelectItem(_snapScrollRect.CurrentPage);
    }
}
