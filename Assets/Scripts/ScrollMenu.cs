﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public abstract class ScrollMenu : MonoBehaviour
{
    /// <summary>
    /// The prefab for the category menu items.
    /// </summary>
    [SerializeField]
    private ScrollMenuItem _itemPrefab;

    /// <summary>
    /// The menu's scroll rect.
    /// </summary>
    private ScrollRect _scrollRect;

    /// <summary>
    /// A dictionary containing the items in the menu.
    /// </summary>
    //private readonly Dictionary<string, ScrollMenuItem> _itemsDict = new Dictionary<string, ScrollMenuItem>();
    private readonly List<ScrollMenuItem> _itemsList = new List<ScrollMenuItem>();

    /// <summary>
    /// The currently selected menu item.
    /// </summary>
    protected ScrollMenuItem _selectedItem;

    /// <summary>
    /// Category Selected event delegates.
    /// </summary>
    public event Action<ScrollMenuItem> OnItemSelected;

    /// <summary>
    /// OnDestroy event handler.
    /// </summary>
    protected virtual void OnDestroy()
    {
        foreach (var item in _itemsList)
        {
            if (item != null)
            {
                // Unregister the listener for the OnClick event of the menu item.
                item.OnItemClicked -= ScrollMenuItem_OnItemClicked;
            }
        }
    }

    /// <summary>
    /// Used for initialization.
    /// </summary>
    protected virtual void Awake()
    {
        _scrollRect = GetComponent<ScrollRect>();
        _selectedItem = null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clickedItem"></param>
    protected virtual void ScrollMenuItem_OnItemClicked(ScrollMenuItem clickedItem)
    {
        OnItemSelected?.Invoke(clickedItem);
    }

    /// <summary>
    /// Selects a item from the menu.
    /// </summary>
    /// <param name="selectedCategory">The selected category menu item.</param>
    protected virtual void SelectItem(ScrollMenuItem item)
    {
        if (item != _selectedItem)
        {
            // Store the selected category.
            _selectedItem = item;
            // Notify anyone interested that a new category was selected.
            OnItemSelected?.Invoke(item);
        }
    }

    /// <summary>
    /// Add categories to the menu.
    /// </summary>
    /// <param itemName="categories">A name for the instantiated item.</param>
    protected ScrollMenuItem InstantiateItem()
    {
        // Instantiate a menu item prefab for each category in the list.
        var newItem = Instantiate(_itemPrefab, _scrollRect.content);
        // Register a listener for the OnClick event of the menu item.
        newItem.OnItemClicked += ScrollMenuItem_OnItemClicked;
        // Store the item.
        _itemsList.Add(newItem);

        return newItem;
    }

    /// <summary>
    /// Select the specified item by name.
    /// </summary>
    /// <param name="itemName">The name of the item to be selected.</param>
    public void SelectItem(int itemIndex)
    {
        if (itemIndex > 0 || itemIndex < _itemsList.Count )
        {
            // Select the item.
            SelectItem(_itemsList[itemIndex]);
        }
    }

    /// <summary>
    /// Removes all the items from the menu.
    /// </summary>
    public void ClearItems()
    {
        foreach (var item in _itemsList)
        {
            DestroyImmediate(item.gameObject);
        }
        _itemsList.Clear();
        _selectedItem = null;
    }
}
