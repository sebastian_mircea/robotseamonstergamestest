﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

/// <summary>
/// Source: https://gist.github.com/ryoojw/26264efba0519ba7817828fa81e2092b
/// This is a version of the code found at the address abowe, stripped down a bit and modified for this test.
/// TODO: It needs a little more fine tuning and cleaning or it can be entirely replaced.
/// </summary>
[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Mask))]
[RequireComponent(typeof(ScrollRect))]
public class SnapScrollRect : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [Tooltip("Set starting page index - starting from 0")]
    [SerializeField]
    private int _startingPage = 0;

    [Tooltip("Threshold time for fast swipe in seconds")]
    [SerializeField]
    private float _fastSwipeThresholdTime = 0.3f;

    [Tooltip("Threshold time for fast swipe in (unscaled) pixels")]
    [SerializeField]
    private int _fastSwipeThresholdDistance = 100;

    [Tooltip("Threshold time for fast swipe in seconds")]
    [SerializeField]
    private float _upSwipeThresholdTime = 0.3f;

    [Tooltip("Threshold time for fast swipe in (unscaled) pixels")]
    [SerializeField]
    private int _upSwipeThresholdDistance = 30;


    [Tooltip("How fast will page lerp to target position")]
    [SerializeField]
    private float _decelerationRate = 10f;

    [Tooltip("Button to go to the previous page (optional)")]
    [SerializeField]
    private GameObject _prevButton;

    [Tooltip("Button to go to the next page (optional)")]
    [SerializeField]
    private GameObject _nextButton;

    public event Action<int> OnSwipeUp;

    public event Action<int> OnSwipeDown;

    // Fast swipes should be fast and short. If too long, then it is not a fast swipe
    private int _fastSwipeThresholdMaxLimit;

    // The ScrollRect component.
    private ScrollRect _scrollRectComponent;
    // The rect of the scroll rect.
    private RectTransform _scrollRectRect;
    // The scroll elements container.
    private RectTransform _container;

    // Number of pages in container
    private int _pageCount;

    private int _currentPage;

    // Whether lerping is in progress or not.
    private bool _lerp;
    // Target lerp position.
    private Vector2 _lerpTo;

    // target position of every page
    private List<Vector2> _pagePositions = new List<Vector2>();

    // Are we draggging?  and where it started
    private bool _dragging;
    // Dragging start timestamp.
    private float _timeStamp;
    // Dragging start position.
    private Vector2 _startPosition;
    private Vector2 _dragPointerStartPosition;

    public int CurrentPage { get => _currentPage; set => _currentPage = value; }

    public void Initialize()
    {
        _pageCount = _container.childCount;
        SetPagePositions();
        SetPage(_startingPage);
    }

    void Start()
    {
        _scrollRectComponent = GetComponent<ScrollRect>();
        _scrollRectRect = GetComponent<RectTransform>();
        _container = _scrollRectComponent.content;        

        _lerp = false;

        Initialize();

        // prev and next buttons
        if (_nextButton)
            _nextButton.GetComponent<Button>().onClick.AddListener(() => { NextScreen(); });

        if (_prevButton)
            _prevButton.GetComponent<Button>().onClick.AddListener(() => { PreviousScreen(); });
    }

    void Update()
    {
        // if moving to target position
        if (_lerp)
        {
            // prevent overshooting with values greater than 1
            float decelerate = Mathf.Min(_decelerationRate * Time.deltaTime, 1f);
            _container.anchoredPosition = Vector2.Lerp(_container.anchoredPosition, _lerpTo, decelerate);
            // time to stop lerping?
            if (Vector2.SqrMagnitude(_container.anchoredPosition - _lerpTo) < 0.25f)
            {
                // snap to target and stop lerping
                _container.anchoredPosition = _lerpTo;
                _lerp = false;
                // clear also any scrollrect move that may interfere with our lerping
                _scrollRectComponent.velocity = Vector2.zero;
            }
        }
    }

    private void SetPagePositions()
    {        
        // screen width in pixels of scrollrect window
        int width = (int)_scrollRectRect.rect.width;
        // center position of all pages
        int offsetX = width / 2;
        // total width
        int containerWidth = width * _pageCount;
        // limit fast swipe length - beyond this length it is fast swipe no more
        _fastSwipeThresholdMaxLimit = width;

        // set width of container
        Vector2 newSize = new Vector2(containerWidth, 0);
        _container.sizeDelta = newSize;
        Vector2 newPosition = new Vector2(containerWidth / 2, 0);
        _container.anchoredPosition = newPosition;

        // delete any previous settings
        _pagePositions.Clear();

        // iterate through all container childern and set their positions
        for (int i = 0; i < _pageCount; i++)
        {
            RectTransform child = _container.GetChild(i).GetComponent<RectTransform>();
            Vector2 childPosition = new Vector2(i * width - containerWidth / 2 + offsetX, 0f);
 
            child.anchoredPosition = childPosition;
            _pagePositions.Add(-childPosition);
        }
    }
    
    private void SetPage(int aPageIndex)
    {
        if (_pageCount > 0)
        {
            aPageIndex = Mathf.Clamp(aPageIndex, 0, _pageCount - 1);
            _container.anchoredPosition = _pagePositions[aPageIndex];
            _currentPage = aPageIndex;
        }
    }
    
    private void LerpToPage(int aPageIndex)
    {
        aPageIndex = Mathf.Clamp(aPageIndex, 0, _pageCount - 1);
        _lerpTo = _pagePositions[aPageIndex];
        _lerp = true;
        _currentPage = aPageIndex;
    }
    
    private void NextScreen()
    {
        LerpToPage(_currentPage + 1);
    }

    private void PreviousScreen()
    {
        LerpToPage(_currentPage - 1);
    }
    
    private int GetNearestPage()
    {
        // based on distance from current position, find nearest page
        Vector2 currentPosition = _container.anchoredPosition;

        float distance = float.MaxValue;
        int nearestPage = _currentPage;

        for (int i = 0; i < _pagePositions.Count; i++)
        {
            float testDist = Vector2.SqrMagnitude(currentPosition - _pagePositions[i]);
            if (testDist < distance)
            {
                distance = testDist;
                nearestPage = i;
            }
        }

        return nearestPage;
    }

    public void OnBeginDrag(PointerEventData aEventData)
    {
        // if currently lerping, then stop it as user is draging
        _lerp = false;
        // not dragging yet
        _dragging = false;
    }
    
    public void OnEndDrag(PointerEventData aEventData)
    {
        // test for fast swipe - swipe that moves only +/-1 item
        if (Time.unscaledTime - _timeStamp < _fastSwipeThresholdTime)
        {
            // Calculate how much was the container's content dragged on the X axis.
            float xDifference = _startPosition.x - _container.anchoredPosition.x;
            // Calculate how much was the container's content dragged on the Y axis.
            //float yDifference = _startPosition.y - _container.anchoredPosition.y;
            float yDifference = _dragPointerStartPosition.y - aEventData.position.y;
            if (Mathf.Abs(xDifference) > _fastSwipeThresholdDistance && Mathf.Abs(xDifference) < _fastSwipeThresholdMaxLimit)
            {
                if (xDifference > 0)
                {
                    NextScreen();
                }
                else
                {
                    PreviousScreen();
                }
            }
            else if (Mathf.Abs(yDifference) > _upSwipeThresholdDistance /*&& Mathf.Abs(yDifference) < _upSwipeThresholdMaxLimit*/)
            {
                if (yDifference > 0)
                {
                    OnSwipeDown?.Invoke(_currentPage);
                }
                else
                {
                    OnSwipeUp?.Invoke(_currentPage);
                }
            }
        }
        else
        {
            // if not fast time, look to which page we got to
            LerpToPage(GetNearestPage());
        }

        _dragging = false;
    }
    
    public void OnDrag(PointerEventData aEventData)
    {
        if (!_dragging)
        {
            // dragging started
            _dragging = true;
            // save time - unscaled so pausing with Time.scale should not affect it
            _timeStamp = Time.unscaledTime;
            // save current position of cointainer
            _startPosition = _container.anchoredPosition;
            _dragPointerStartPosition = aEventData.position;
        }
    }
}
