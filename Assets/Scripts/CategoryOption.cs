﻿using UnityEngine;

/// <summary>
/// A scriptable object to keep the options of a category.
/// </summary>
[CreateAssetMenu(fileName = "CategoryOption", menuName = "ScriptableObjects/CategoryOption", order = 1)]
public class CategoryOption : ScriptableObject
{
    /// <summary>
    /// The option name.
    /// </summary>
    [SerializeField]
    public string _optionName;

    /// <summary>
    /// The sprite coresponding to the option.
    /// </summary>
    [SerializeField]
    public Sprite _image;

    /// <summary>
    /// Getter for the option name.
    /// </summary>
    public string OptionName
    {
        get { return _optionName; }
    }

    /// <summary>
    /// Getter for the option sprite.
    /// </summary>
    public Sprite Image
    {
        get { return _image; }
    }
}
