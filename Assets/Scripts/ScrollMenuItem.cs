﻿using System;
using UnityEngine;

/// <summary>
/// A base class for the scroll menu items.
/// </summary>
public abstract class ScrollMenuItem : MonoBehaviour
{
    /// <summary>
    /// Item Clicked event.
    /// </summary>
    public event Action<ScrollMenuItem> OnItemClicked;

    /// <summary>
    /// Getter and setter for the category name.
    /// </summary>
    public virtual string ItemName
    {
        get;
        set;
    }

    /// <summary>
    /// Item initialization method.
    /// </summary>
    protected abstract void Initialize();

    /// <summary>
    /// Calls the observers for the Item OnClick event.
    /// </summary>
    protected void CallOnItemClickedEventObservers()
    {
        // Notify anyone interested that a category was selected.
        OnItemClicked?.Invoke(this);
    }

    /// <summary>
    /// Used for initialization.
    /// </summary>
    private void Awake()
    {
        Initialize();
    }
}
