﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Options menu item implementation.
/// </summary>
public class OptionsMenuItem : ScrollMenuItem
{
    /// <summary>
    /// A reference to the text control that displays the item name.
    /// </summary>
    private Text _textLabel;

    /// <summary>
    /// The 
    /// </summary>
    private Image _image;

    /// <summary>
    /// Getter and setter for the category name.
    /// </summary>
    public override string ItemName
    {
        get { return _textLabel.text; }
        set { _textLabel.text = value; }
    }

    /// <summary>
    /// Getter and setter for the item image.
    /// </summary>
    public Sprite ItemSprite
    {
        get { return _image.sprite; }
        set { _image.sprite = value; }
    }

    /// <summary>
    /// Initialize the options menu item.
    /// </summary>
    protected override void Initialize()
    {
        _textLabel = GetComponentInChildren<Text>(true);

        _image = GetComponentInChildren<Image>(true);
    }
}
