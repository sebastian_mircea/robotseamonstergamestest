﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A scrollable menu for the categories.
/// </summary>
[RequireComponent(typeof(ToggleGroup))]
public class CategoryMenu : ScrollMenu
{
    /// <summary>
    /// The toggle group for the menu items.
    /// </summary>
    private ToggleGroup _toggleGroup;

    /// <summary>
    /// Used for initialization.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        _toggleGroup = GetComponent<ToggleGroup>();
    }


    /// <summary>
    /// Selects a item from the menu.
    /// </summary>
    /// <param name="selectedItem">The selected menu item.</param>
    protected override void SelectItem(ScrollMenuItem item)
    {
        // Let the base class do its job.
        base.SelectItem(item);
        // Get the category.
        var categoryMenuItem = item as CategoryMenuItem;
        if (categoryMenuItem == null)
        {
            return;
        }
        // Turn the toggle on.
        categoryMenuItem.MenuItemToggle.isOn = true;
    }


    /// <summary>
    /// Add categories to the menu.
    /// </summary>
    /// <param name="categories">The list of categories to add.</param>
    public void AddCategories(List<string> categories)
    {
        foreach (var category in categories)
        {
            // Instantiate a menu item prefab for each category in the list.
            var newCategoryMenuItem = InstantiateItem() as CategoryMenuItem;
            // Set the name of the category.
            newCategoryMenuItem.ItemName = category;
            // Add the menu item toggle to the toggle group.
            newCategoryMenuItem.MenuItemToggle.group = _toggleGroup;
        }
    }

    /// <summary>
    /// Item clicked event handler.
    /// </summary>
    /// <param name="clickedItem">The item that was clicked.</param>
    protected override void ScrollMenuItem_OnItemClicked(ScrollMenuItem clickedItem)
    {
        // Get the clicked category.
        var item = clickedItem as CategoryMenuItem;
        // Let the base class do its job.
        base.ScrollMenuItem_OnItemClicked(item);
        // Set the toggle on.
        item.MenuItemToggle.isOn = true;
    }
}
